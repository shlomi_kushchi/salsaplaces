from django.contrib import admin

# Register your models here.
from .models import Post

class PostModelAdmin(admin.ModelAdmin):
	class Meta:
		model = Post

	list_display = ["title", "updated", "timestamp", "author"]
	list_filter = ["timestamp", "updated"]
	search_fields = ["title", "content"]



admin.site.register(Post, PostModelAdmin) # this is registering the app to the admin site
