from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from salsaplaces.posts.forms import PostForm
from salsaplaces.posts.models import Post


def home_view(request):
	posts = Post.objects.all()
	return render(request, "landing.html", {"posts": posts})

def list_posts(request):
	posts = Post.objects.all()
	return render(request, "all_posts.html", {"posts": posts})


def view_post(request, id):
	post = Post.objects.get(id=id)
	return render(request, "view_post.html", {"post": post})

def create_post(request):
	"""
	create post will serve for GET and POST.
	the GET method will give the user the empty form
	the POST method will submit the new post
	:param request:
	:return:
	"""
	form = PostForm(request.POST or None) # request.POST will make sure all required field were filled. None is for GET the form.
	if request.method == "POST":
		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()
			messages.success(request, "Succesfully created")
			return HttpResponseRedirect(instance.get_absolute_url())
	return render(request, "create_post.html", {"form": form, "update_post": False})

def edit_post(request, id):
	"""
	update a post
	:param id: post id
	:return:
	"""
	post_instance = get_object_or_404(Post, id=id)
	# after loading a post we put it inside our form, thus loading a form for edi
	form = PostForm(request.POST or None, instance=post_instance)
	if request.method == "POST":
		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()
			messages.success(request, "Succesfully Edited")
			return HttpResponseRedirect(instance.get_absolute_url())
	return render(request, "create_post.html", {"form": form, "update_post": True})

def delete_post(request, id):
	"""
	delete the post
	:param request:
	:param id:
	:return:
	"""
	post_instance = get_object_or_404(Post, id=id)
	post_instance.delete()
	messages.success(request, "Succesfullt deleted")
	return redirect("posts:home")