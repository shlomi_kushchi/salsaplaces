from django import forms


from salsaplaces.posts.models import Post


class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = [
			"title",
			"content",
		]