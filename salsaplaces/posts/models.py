from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.

class Post(models.Model):
	# app_label = "salsaplaces.posts"
	title = models.CharField(max_length=255)
	content = models.TextField()
	timestamp = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	author = models.CharField(max_length=255)

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("posts:view_post", kwargs={"id": self.id})

	def get_edit_url(self):
		return reverse("posts:edit_post", kwargs={"id": self.id})

	def get_delete_url(self):
		return reverse("posts:delete_post", kwargs={"id": self.id})

	def back_to_posts(self):
		return reverse("posts:list_posts", kwargs={})

