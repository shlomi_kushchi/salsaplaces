from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from salsaplaces.posts.views import (home_view, view_post, create_post, edit_post, delete_post, list_posts)

urlpatterns = [
    url(r'^$', home_view, name="home"),
    url(r'^list/$', list_posts, name="list_posts"),
    url(r'^post/(?P<id>[0-9]+)$', view_post, name="view_post"),
    url(r'^create/$', create_post, name="create_post"),
    url(r'^(?P<id>\d+)/edit$', edit_post, name="edit_post"),
    url(r'^(?P<id>\d+)/delete', delete_post, name="delete_post"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)