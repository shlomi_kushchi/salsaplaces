from django.conf.urls import include, url
from django.contrib import admin
import salsaplaces.posts.urls

# options for urls:
#     https://docs.djangoproject.com/en/1.9/topics/class-based-views/intro/
#
# function based view:
#     def my_view(request):
#         if request.method == 'GET':
#             # <view logic>
#             return HttpResponse('result')
#
#     => url(r'^$', "my_view"),
#
#
# class based view:
#
#     class MyView(View):
#     def get(self, request):
#         # <view logic>
#         return HttpResponse('result')
#
#     => url(r'^about/', MyView.as_view()),


urlpatterns = [url(r'^admin/', include(admin.site.urls)),
               url(r'^', include(salsaplaces.posts.urls, namespace="posts")),
]
