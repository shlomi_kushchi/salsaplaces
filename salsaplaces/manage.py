#!/usr/bin/env python
import os
import sys


# to be able to run on cloud9:
import platform
if "linux" in platform.platform().lower():
    sys.path.insert(0,"/home/ubuntu/workspace")

"""
Steps to run on cloud9
1. create and run your virtual env:
   a. create: virtualenv venv
   b. activate:
      - cd venv
      - source bin/activate
   c. install requirements
      - cd .. ( back to workspace home )
      - pip install -r requirements.txt
2. make sure the db is created and synced:
  a. python salsaplaces/manage.py makemigrations
  b. python salsaplaces/manage.py migrate
3. run server:  
python salsaplaces/manage.py runserver 0.0.0.0:8080

your cwd should be the workspace
"""

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "setup.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
